import { formComponent } from 'devfractal-crud'
import React from 'react'
import { req, string, TypeOf } from 'technoidentity-utils'
import { Simple } from 'devfractal-simple'
import { Columns, Column, Section } from 'devfractal-ui-core'
import { Link } from 'react-router-dom'

const LoginValues = req({
  userName: string,
  password: string,
})

type LoginValues = TypeOf<typeof LoginValues>

export const LoginFormInner = formComponent(
  LoginValues,
  ({ initial, onSubmit }) => (
    <Section>
      <Columns columnCentered>
        <Column size="half">
          <Simple.Form initialValues={initial} onSubmit={onSubmit}>
            <Simple.Text name="userName" />
            <Simple.Password name="password" />
            <Simple.FormButtons />
            <Simple.Debug />
          </Simple.Form>
        </Column>
      </Columns>
    </Section>
  ),
)

export const LoginForm = () => (
  <LoginFormInner onSubmit={async values => console.log(values)} />
)
