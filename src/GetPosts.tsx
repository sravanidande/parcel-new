import {
  APIQuery,
  http as httpAPI,
  toJSONServerQuery,
  rest,
} from 'technoidentity-devfractal'
import {
  readonlyArray,
  req,
  string,
  TypeOf,
  boolean,
  number,
} from 'technoidentity-utils'

const http = httpAPI({ baseURL: 'https://jsonplaceholder.typicode.com' })
const postType = req({ title: string, body: string })
type postType = TypeOf<typeof postType>

export const getPosts = async () =>
  console.log(await http.get({ resource: 'posts' }, readonlyArray(postType)))

export const getOne = async () =>
  console.log(await http.get({ resource: 'posts', path: '1' }, postType))

export const PostOne = async () =>
  console.log(
    await http.post(
      { resource: 'posts' },
      { title: 'learn git', body: 'manage project properly' },
      postType,
    ),
  )

export const putOne = async () =>
  console.log(
    await http.put(
      { resource: 'posts', path: '2' },
      { title: 'learn mongoose', body: 'server side programming' },
      postType,
    ),
  )

export const deleteOne = async () =>
  console.log(await http.del({ resource: 'posts', path: '1' }))

const TodoType = req({ id: number, title: string, completed: boolean })

type TodoType = TypeOf<typeof TodoType>

const query: APIQuery<TodoType> = {
  filter: { completed: true },
}

console.log(toJSONServerQuery(TodoType, query))

const todoAPI = rest(
  TodoType,
  'id',
  {
    resource: 'todos',
    baseURL: 'https://jsonplaceholder.typicode.com',
  },
  toJSONServerQuery,
)

export const filteringData = async () =>
  console.log(
    await todoAPI.list({
      filter: { completed: true },

      // query: 'completed=true&title_like=quo&_sort=title&_order=asc',
    }),
  )
