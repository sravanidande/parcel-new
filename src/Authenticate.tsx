import React from 'react'
import { Button, Title } from 'technoidentity-devfractal'

interface LoginProps {
  onLogin(): void
}

export const Login: React.FC<LoginProps> = ({ onLogin }) => (
  <>
    <Title>Please SignUp</Title>
    <Button onClick={onLogin}>Login</Button>
  </>
)

interface LogoutProps {
  onLogout(): void
}

export const Logout: React.FC<LogoutProps> = ({ onLogout }) => (
  <>
    <Title>Welcome back</Title>
    <Button onClick={onLogout}>Logout</Button>
  </>
)

export const Authenticate: React.FC = () => {
  const [loggedIn, setLoggedIn] = React.useState(false)
  const handleAuth = () => setLoggedIn(!loggedIn)

  return (
    <>
      {loggedIn ? (
        <Logout onLogout={handleAuth} />
      ) : (
        <Login onLogin={handleAuth} />
      )}
    </>
  )
}
