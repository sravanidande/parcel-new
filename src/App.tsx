import 'bulma/css/bulma.css'
import React from 'react'
import MyComponent from '../src/MuExample'
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core'

const theme = createMuiTheme()

export const ThemeOwn = () => (
  <MuiThemeProvider theme={theme}>
    <MyComponent />
  </MuiThemeProvider>
)

export const App = () => <ThemeOwn />

// export const sumOfRange = (start: number, stop: number) => {
//   let sum = 0
//   for (let i = start; i <= stop; i += 1) {
//     sum += i
//   }
//   return sum
// }
// console.log(sumOfRange(1, 10))

// export const reverseArray = (arr: number[]) => {
//   const result = []
//   // tslint:disable-next-line: prefer-for-of
//   for (let i = arr.length - 1; i >= 0; i -= 1) {
//     result.push(arr[i])
//   }
//   return result
// }

// console.log(reverseArray([1, 2, 3]))
