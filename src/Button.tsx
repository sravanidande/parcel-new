import React from 'react'
import { Button } from 'devfractal-ui-core'

export const Toggle = () => {
  const [toggle, setToggle] = React.useState(true)

  const handleClick = () => setToggle(!toggle)
  return <Button onClick={handleClick}>{toggle ? 'On' : 'off'}</Button>
}
