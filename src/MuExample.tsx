import React from 'react'
import Button from '@material-ui/core/Button'
import Icon from '@material-ui/core/Icon'
import { TextField, InputAdornment, withStyles } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'

const MuButton: React.FC = () => (
  <>
    <Button variant="contained" color="default">
      <Icon>location_on</Icon>
      Find Me
    </Button>
    <TextField label="Basic TextField" />
    <TextField placeholder="Enter Text" label="Basic TextField" />
    <TextField
      label="Length"
      InputProps={{
        endAdornment: <InputAdornment position="start">in.</InputAdornment>,
      }}
    />
  </>
)

// const styles = (theme: any) => ({
//   container: {
//     backgroundColor: 'blue',
//     color: 'red',
//     width: '75%',
//     height: 5 * theme.spacing.unit,
//   },
// })

const styles = makeStyles(theme => ({
  container: {
    backgroundColor: 'blue',
    color: 'red',
    width: '75%',
    height: '50px',
  },
}))

const MyComponent = () => {
  const classes = styles()
  return (
    <div className={classes.container}>
      <p>dgghsd ZHDisd zhdsuhz</p>
    </div>
  )
}

export default MyComponent
