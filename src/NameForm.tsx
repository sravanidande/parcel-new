import React from 'react'

export const NameForm: React.FC = () => {
  const [value, setValue] = React.useState('coconut')
  const [checked, setChecked] = React.useState(false)
  const handleChange = (e: any) => {
    console.log(e.target)
    setValue(e.target.value)
  }
  const handleChecked = (e: any) => {
    console.log(e.target)

    setChecked(!checked)
  }

  const handleSubmit = (evt: any) => {
    evt.preventDefault()
    console.log(value, 'checked:', checked)
  }
  return (
    <form>
      <label>Name: </label>
      <select onChange={handleChange}>
        <option value="coconut">Coconut</option>
        <option value="grapeFruit">Grapes</option>
        <option value="lemon">Lemon</option>
      </select>
      <br />
      <input
        type="checkbox"
        name="checked"
        checked={checked}
        onChange={handleChecked}
      />

      <br />
      <button type="submit" onClick={handleSubmit}>
        submit
      </button>
    </form>
  )
}
