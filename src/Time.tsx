import { Title } from 'devfractal-ui-core'
import React, { useEffect } from 'react'

export interface TimeProps {
  readonly date: Date
}

export const Time: React.FC<TimeProps> = ({ date }) => (
  <Title>it is {date.toLocaleTimeString()}</Title>
)

const today = new Date()

export const UpdatedTime: React.FC = () => {
  const [time, setTime] = React.useState(today)

  useEffect(() => {
    const timeOut = setInterval(() => setTime(new Date()), 1000)
  }, [])

  return <Time date={time} />
}
